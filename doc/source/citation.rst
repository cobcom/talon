.. _citation:

====================
How to cite talon
====================
If you use talon in your research, please cite the package in the following
format.

    Matteo Frigo, Mauro Zucchelli, Rachid Deriche, Samuel Deslauriers-Gauthier.
    "TALON: Tractograms As Linear Operators in Neuroimaging."
    `⟨hal-03116143⟩ <https://hal.archives-ouvertes.fr/hal-03116143>`_

.. code:: bibtex

    @misc{cobcomtalon,
          author = {Frigo, Matteo and Zucchelli, Mauro and Deriche, Rachid and
          Deslauriers-Gauthier, Samuel},
          title = {TALON: Tractograms As Linear Operators in Neuroimaging},
          howpublished = {CoBCoM},
          url = {https://hal.archives-ouvertes.fr/hal-03116143},
          year = {2021}
         }

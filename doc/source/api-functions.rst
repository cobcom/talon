.. _api-functions:

==============
Functions
==============

.. automodule:: talon
    :members: operator, concatenate, diagonalize, regularization, solve, voxelize, zeros

.. automodule:: talon.utils
    :members: check_pattern_iw, concatenate_giw, directions, mask_data
=================================
Welcome to talon's documentation!
=================================
.. image:: https://gitlab.inria.fr/cobcom/talon/badges/master/pipeline.svg
    :target: https://gitlab.inria.fr/cobcom/talon/-/commits/master"
    :alt: Pipeline Status

.. image:: https://gitlab.inria.fr/cobcom/talon/badges/master/coverage.svg
    :target: https://gitlab.inria.fr/cobcom/talon/-/commits/master
    :alt: Coverage Report

.. image:: https://readthedocs.org/projects/cobcom-talon/badge/?version=latest
    :target: https://cobcom-talon.readthedocs.io/en/latest/?badge=latest
    :alt: Documentation Status

`talon` is a pure Python package that implements Tractograms As Linear
Operators in Neuroimaging.

The software provides the ``talon`` Python module, which includes all the
functions and tools that are necessary for **filtering** a tractogram.
In particular, specific functions are devoted to:

* Transforming a tractogram into a linear operator.
* Solving the inverse problem associated to the filtering of a tractogram.
* Perform these operations on a GPU.

The package is `available at Pypi <https://pypi.org/project/cobcom-talon/>`_
and can be easily installed from the command line.

.. code:: bash

    pip install cobcom-talon

Talon is a free software released under
`MIT license <https://gitlab.inria.fr/cobcom/talon/-/blob/master/LICENSE>`_
and the documentation is available on
`Read the Docs <https://cobcom-talon.readthedocs.io/>`_.


Getting help
============
The preferred way to get assistance in running code that uses ``talon`` is
through the issue system of the
`Gitlab repository <https://gitlab.inria.fr/cobcom/talon>`_ where the source
code is available.
Developers and maintainers frequently check newly opened issues and will be
happy to help you.


Contributing guidelines
=======================
The development happens in the ``devel`` branch of the
`Gitlab repository <https://gitlab.inria.fr/cobcom/talon>`_, while the
``master`` is kept for the stable releases only.
We will consider only merge requests towards the ``devel`` branch.


How to cite
===========
If you publish works using talon, please cite us as indicated here:

    Matteo Frigo, Mauro Zucchelli, Rachid Deriche, Samuel Deslauriers-Gauthier.
    "TALON: Tractograms As Linear Operators in Neuroimaging." CoBCoM, 2021.
    https://hal.archives-ouvertes.fr/hal-03116143

In section :ref:`citation` you will find the Bibtex entry.


.. toctree::
    :maxdepth: 2
    :caption: Install and Get Started

    installation
    getting-started

.. toctree::
    :maxdepth: 2
    :caption: Command Line Interface

    command-line-interface


.. toctree::
    :maxdepth: 2
    :caption: Concepts

    inverse-problem
    concatenate
    diagonalize


.. toctree::
    :maxdepth: 2
    :caption: API documentation

    api-functions
    api-classes
    api-cli

.. toctree::
    :maxdepth: 1
    :caption: About the project

    citation
    contributors
    license
    funding
